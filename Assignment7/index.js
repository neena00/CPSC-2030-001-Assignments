var obj1 = {name:"Claude Wallace", side:"Edinburgh Army", unit:"Ranger Corps, Squad E", rank:"First Lieutenant", role:" Tank Commander", description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss01.jpg"};
            var obj2 = {name:"Riley Miller", side:"Edinburgh Army", unit:"Federate Joint Ops", rank:"Second Lieutenant", role:"Artillery Advisor", description:"Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss02.jpg"};
            var obj3 = {name:"Raz", side:"Edinburgh Army", unit:"Ranger Corps, Squad E", rank:"First Lieutenant", role:" Tank Commander", description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss03.jpg"};
            var obj4 = {name:"Kai Schulen", side:"Edinburgh Army", unit:"Ranger Corps, Squad E", rank:"First Lieutenant", role:" Tank Commander", description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss04.jpg"};
            var obj5 = {name:"Angelica Farnaby", side:"Edinburgh Army", unit:" N/A", rank:" N/A", role:" N/A", description:"A chipper civilian girl who stumbled upon Squad E through strange circumstances. Nicknamed Angie, she is beloved by the entire squad for her eagerness to help. She seems to be suffering from amnesia, and can only remember her own name.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss05.jpg"};
            var obj6 = {name:"Minerva Victor", side:"Edinburgh Army", unit:"Ranger Corps, Squad E", rank:"First Lieutenant", role:"Senior Commander", description:"Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss11.jpg"};
            var obj7 = {name:"Karen Stuart", side:"Edinburgh Army", unit:"Ranger Corps, Squad F", rank:"First Lieutenant", role:" Commander", description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss12.jpg"};
            var obj8 = {name:"Ragnarok", side:"Edinburgh Army", unit:"Ranger Corps, Squad E", rank:"First Lieutenant", role:" Tank Commander", description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss13.jpg"};
            var obj9 = {name:"Miles Arbeck", side:"Edinburgh Army", unit:"Ranger Corps, Squad E", rank:"Sergeant", role:" Tank Commander", description:"Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.", imageUrl:"http://valkyria.sega.com/img/character/img15.png"};
            var obj10 = {name:"Dan Bentley", side:"Edinburgh Army", unit:"Ranger Corps, Squad E", rank:"Private First Class", role:" APC Operator", description:"Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.", imageUrl:"http://valkyria.sega.com/img/character/chara-ss16.jpg"};
            var obj = new Array(obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, obj9, obj10);
            var arr = "";
            for (var i = 0; i < obj.length; i++)
            {
            arr += "<li onclick='go(" + i + ")'>" + obj[i].name + "</li>";
            }

var squad =  new Array();
var j=0;
            function go(i)
            {
                if(j<5)
                {
                    squad[j]=obj[i];
                    j++;
                }
                
                var s="";
               for (var i = 0; i <j; i++)
                {
                    s+=squad[i].name+"<br>";
                } 
                    document.getElementById("squad").innerHTML = s;
                    document.getElementById("image").src = obj[i].imageUrl;
                    document.getElementById("profile").innerHTML = "side: "+obj[i].side+"<br> unit: "+obj[i].unit+"<br> rank: "+obj[i].rank+"<br> role: "+obj[i].role ;
            }
            document.getElementById("batalion").innerHTML = arr;
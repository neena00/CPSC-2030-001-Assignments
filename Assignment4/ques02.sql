SELECT distinct pt.name 
FROM pokemonTYPE AS pt
INNER JOIN resistantto r ON pt.type1 = r.type or pt.type2 = r.type
INNER JOIN vulnerableto v ON pt.type1 = v.type or pt.type2 = v.type
WHERE ( v.vulnerableto1= 'Ground' or v.vulnerableto2= 'Ground' or v.vulnerableto3 = 'Ground' or v.vulnerableto4 = 'Ground' or v.vulnerableto5 = 'Ground') AND ( r.resistantTo1= 'Steel' or r.resistantTo2= 'Steel' or r.resistantTo3= 'Steel' or r.resistantTo4= 'Steel' or r.resistantTo5= 'Steel' or r.resistantTo6= 'Steel' or r.resistantTo7= 'Steel' or r.resistantTo8= 'Steel' or r.resistantTo9= 'Steel' or r.resistantTo10= 'Steel' or r.resistantTo11= 'Steel')  
ORDER BY `name` ASC
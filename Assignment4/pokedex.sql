-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:4001
-- Generation Time: Oct 12, 2018 at 04:31 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pokedex`
--

-- --------------------------------------------------------

--
-- Table structure for table `pokemon`
--

CREATE TABLE `pokemon` (
  `Nat.#` int(11) NOT NULL,
  `Hoenn#` int(11) NOT NULL,
  `Name` text NOT NULL,
  `HP` int(11) NOT NULL,
  `Atk` int(11) NOT NULL,
  `Def` int(11) NOT NULL,
  `SAt` int(11) NOT NULL,
  `SDf` int(11) NOT NULL,
  `Spd` int(11) NOT NULL,
  `BST` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemon`
--

INSERT INTO `pokemon` (`Nat.#`, `Hoenn#`, `Name`, `HP`, `Atk`, `Def`, `SAt`, `SDf`, `Spd`, `BST`) VALUES
(387, 0, 'Turtwig', 55, 68, 64, 45, 55, 31, 318),
(388, 0, 'Grotle', 75, 89, 85, 55, 65, 36, 405),
(389, 0, ' Torterra', 95, 109, 105, 75, 85, 56, 525),
(390, 0, 'Chimchar', 44, 58, 44, 58, 44, 61, 309),
(391, 0, 'Monferno', 64, 78, 52, 78, 52, 81, 405),
(392, 0, 'Infernape', 76, 104, 71, 104, 71, 108, 534),
(393, 0, 'Piplup', 53, 51, 53, 61, 56, 40, 314),
(394, 0, 'Prinplup ', 64, 66, 68, 81, 76, 50, 405),
(396, 0, 'Starly', 40, 55, 30, 30, 30, 60, 245),
(397, 0, 'Staravia', 55, 75, 50, 40, 40, 80, 340),
(398, 0, 'Staraptor', 85, 120, 70, 50, 60, 100, 485),
(399, 0, 'Bidoof', 59, 45, 40, 35, 40, 31, 250),
(400, 0, 'Bibarel', 79, 85, 60, 55, 60, 71, 410),
(401, 0, 'Kricketune', 77, 85, 51, 55, 51, 65, 384),
(403, 0, 'Shinx', 45, 65, 34, 40, 34, 45, 263),
(404, 0, 'Luxio', 60, 85, 49, 60, 49, 60, 363),
(405, 0, 'Luxray', 80, 120, 79, 95, 79, 70, 523),
(406, 97, 'Budew', 40, 30, 35, 50, 70, 55, 280),
(407, 99, 'Roserade', 60, 70, 65, 125, 105, 90, 515),
(410, 0, 'Shieldon', 30, 42, 118, 42, 88, 30, 350),
(411, 0, 'Bastiodon', 60, 52, 168, 47, 138, 30, 495),
(412, 0, 'Burmy', 40, 29, 45, 29, 45, 36, 224),
(413, 0, 'Wormadam', 60, 59, 85, 79, 105, 36, 424),
(414, 0, 'Mothim', 70, 94, 50, 94, 50, 66, 424),
(415, 0, 'Combee', 30, 30, 42, 30, 42, 70, 244),
(416, 0, 'Vespiquen', 70, 80, 102, 80, 102, 40, 474),
(417, 0, 'Pachirisu', 60, 45, 70, 45, 90, 95, 405),
(418, 0, 'Buizel', 55, 65, 35, 60, 30, 85, 330),
(419, 0, 'Floatzel', 85, 105, 55, 85, 50, 115, 495),
(420, 0, 'Cherubi', 45, 35, 45, 62, 53, 35, 275),
(421, 0, 'Cherrim', 70, 60, 70, 87, 78, 85, 450),
(422, 0, 'Shellos', 76, 48, 48, 57, 62, 34, 325),
(423, 0, 'Gastrodon', 111, 83, 68, 92, 82, 39, 475),
(424, 0, 'Ambipom', 75, 100, 66, 60, 66, 115, 482),
(425, 0, 'Drifloon', 90, 50, 34, 60, 44, 70, 348),
(426, 0, 'Drifblim', 150, 80, 44, 90, 54, 80, 498),
(427, 0, 'Buneary', 55, 66, 44, 44, 56, 85, 350),
(428, 0, 'Lopunny', 65, 76, 84, 54, 96, 105, 480),
(429, 0, 'Mismagius', 60, 60, 60, 105, 105, 105, 495),
(430, 0, 'Honchkrow', 100, 125, 52, 105, 52, 71, 505),
(431, 0, 'Glameow', 49, 55, 42, 42, 37, 85, 310),
(432, 0, 'Purugly', 71, 82, 64, 64, 59, 112, 452),
(433, 157, 'Chingling', 45, 30, 50, 65, 50, 45, 285),
(434, 0, 'Stunky', 63, 63, 47, 41, 41, 74, 329),
(435, 0, 'Skuntank', 103, 93, 67, 71, 61, 84, 479),
(436, 0, 'Bronzor', 57, 24, 86, 24, 86, 23, 300),
(437, 0, 'Bronzong', 67, 89, 116, 79, 116, 33, 500),
(438, 0, 'Bonsly', 50, 80, 95, 10, 45, 10, 290),
(439, 0, 'Mimr Jr.', 20, 25, 45, 70, 90, 60, 310),
(440, 0, 'Happiny', 100, 5, 5, 15, 65, 30, 220),
(441, 0, 'Chatot', 76, 65, 45, 92, 42, 91, 411),
(442, 0, 'Spiritomb', 50, 92, 108, 92, 108, 35, 485),
(443, 0, 'Gible', 58, 70, 45, 40, 45, 42, 300),
(444, 0, 'Gabite ', 68, 90, 65, 50, 55, 82, 410),
(445, 0, 'Garchomp', 108, 130, 95, 80, 85, 102, 600),
(446, 0, 'Munchlax', 135, 85, 40, 40, 85, 5, 390),
(447, 0, 'Riolu', 40, 70, 40, 35, 40, 60, 285),
(448, 0, 'Lucario', 70, 110, 70, 115, 70, 90, 525),
(449, 0, 'Hippopotas', 68, 72, 78, 38, 42, 32, 330),
(450, 0, 'Hippowdon', 108, 112, 118, 68, 72, 47, 525),
(451, 0, 'Skorupi', 40, 50, 90, 30, 55, 65, 330),
(452, 0, 'Drapion', 70, 90, 110, 60, 75, 95, 500),
(453, 0, 'Croagunk', 48, 61, 40, 61, 40, 50, 300),
(454, 0, 'Toxicroak', 83, 106, 65, 86, 65, 85, 490),
(455, 0, 'Carnivine', 74, 100, 72, 90, 72, 46, 454),
(456, 0, 'Finneon', 49, 49, 56, 49, 61, 66, 330),
(457, 0, 'Lumineon', 69, 69, 76, 69, 86, 91, 460),
(458, 0, 'Mantyke', 45, 20, 50, 60, 120, 50, 345),
(459, 0, 'Snover', 60, 62, 50, 62, 60, 40, 334),
(460, 0, 'Abomasnow', 90, 92, 75, 92, 85, 60, 494),
(461, 0, 'Weavile', 70, 120, 65, 45, 85, 125, 510),
(462, 86, 'Magnezone', 70, 70, 115, 130, 90, 60, 535),
(448, 0, 'Mega Lucario', 70, 145, 88, 140, 70, 112, 625),
(460, 0, 'Mega Abomasnow', 90, 132, 105, 132, 105, 30, 594),
(448, 0, 'Lucario', 70, 110, 70, 115, 70, 90, 525),
(487, 0, 'Giratina', 150, 100, 120, 100, 120, 90, 680);

-- --------------------------------------------------------

--
-- Table structure for table `pokemontype`
--

CREATE TABLE `pokemontype` (
  `Name` text NOT NULL,
  `Type1` text,
  `Type2` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pokemontype`
--

INSERT INTO `pokemontype` (`Name`, `Type1`, `Type2`) VALUES
('Turtwig', 'Grass', NULL),
('Grotle', 'Grass', NULL),
('Torterra', 'Grass', 'Ground'),
('Chimchar', 'Fire', NULL),
('Moneferno', 'Fire', 'Fight'),
('Infernape', 'Fire', 'Fight'),
('Piplup', 'Water', NULL),
('Prinplup', 'Water', NULL),
('Empoleon', 'Water', 'Steel'),
('Starly', 'Normal', 'Flying'),
('Staravia', 'Normal', 'Flying'),
('Staraptor', 'Normal', 'Flying'),
('Bidoof', 'Normal', NULL),
('Bibarel', 'Normal', 'Water'),
('Kricketot', 'Bug', NULL),
('Kricketune', 'Bug', NULL),
('Shinx', 'Electric', NULL),
('Luxio', 'Electric', NULL),
('Luxray', 'Electric', NULL),
('Budew', 'Grass', 'Poison'),
('Roserade', 'Grass', 'Poison'),
('Cranidos', 'Rock', NULL),
('Rampardos', 'Rock', NULL),
('Shieldon', 'Rock', 'Steel'),
('Bastiodon', 'Rock', 'Steel'),
('Burmy', 'Bug', NULL),
('Wormadam', 'Bug', 'Grass'),
('Mothim', 'Bug', 'Flying'),
('Combee', 'Bug', 'Flying'),
('Vespiquen', 'Bug', 'Flying'),
('Pachirisu', 'Electric', NULL),
('Buizel', 'Water', NULL),
('Floatzel', 'Water', NULL),
('Cherubi', 'Grass', NULL),
('Cherrium', 'Grass', NULL),
('Shellos', 'Water', NULL),
('Gastrodon', 'Water', 'Ground'),
('Ambipom', 'Normal', NULL),
('Drifloon', 'Ghost', 'Flying'),
('Buneary', 'Normal', NULL),
('Lopunny', 'Normal', NULL),
('Mismagius', 'Ghost', NULL),
('Honchkrow', 'Dark', 'Flying'),
('Glameow', 'Normal', NULL),
('Purugly', 'Normal', NULL),
('Chingling', 'Psychc', NULL),
('Stunky', 'Poison', 'Dark'),
('Skuntank', 'Poison', 'Dark'),
('Bronzor', 'Steel', 'Psychc'),
('Bronzong', 'Steel', 'Psychc'),
('Bonsly', 'Rock', NULL),
('Mime Jr.', 'Psychc', 'Fairy'),
('Happiny', 'Normal', NULL),
('Chatot', 'Normal', 'Flying'),
('Spiritomb', 'Ghost', 'Dark'),
('Gible', 'Dragon', 'Ground'),
('Gabite', 'Dragon', 'Ground'),
('Garchomp', 'Dragon', 'Ground'),
('Munchlax', 'Normal', NULL),
('Riolu', 'Fight', NULL),
('Lucario', 'Fight', 'Steel'),
('Hippopotas', 'Ground', NULL),
('Hippowdon', 'Ground', NULL),
('Skorupi', 'Poison', 'Bug'),
('Drapion', 'Poison', 'Dark'),
('Croagunk', 'Poison', 'Fight'),
('Toxicroak', 'Poison', 'Fight'),
('Carnivine', 'Grass', NULL),
('Finneon', 'Water', NULL),
('Lumineon', 'Water', NULL),
('Mantyke', 'Water', 'Flying'),
('Snover', 'Grass', 'Ice'),
('Abomasnow', 'Grass', 'Ice'),
('Weavile', 'Dark', 'Ice'),
('Magnezone', 'Electric', 'Steel'),
('Mega Abomasnow', 'Grass', 'Ice'),
('Mega Lucario', 'Fight', 'Steel'),
('Lucario', 'Fight', 'Steel'),
('Giratina', 'Ghost', 'Dragon');

-- --------------------------------------------------------

--
-- Table structure for table `resistantto`
--

CREATE TABLE `resistantto` (
  `Type` text NOT NULL,
  `resistantTo1` text,
  `resistantTo2` text,
  `resistantTo3` text,
  `resistantTo4` text,
  `resistantTo5` text,
  `resistantTo6` text,
  `resistantTo7` text,
  `resistantTo8` text,
  `resistantTo9` text,
  `resistantTo10` text,
  `resistantTo11` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resistantto`
--

INSERT INTO `resistantto` (`Type`, `resistantTo1`, `resistantTo2`, `resistantTo3`, `resistantTo4`, `resistantTo5`, `resistantTo6`, `resistantTo7`, `resistantTo8`, `resistantTo9`, `resistantTo10`, `resistantTo11`) VALUES
('Normal', 'Ghost', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Fighting', 'Rock', 'Bug', 'Dark', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Flying', 'Fighting', 'Ground', 'Bug', 'Grass', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Poison', 'Fighting', 'Poison', 'Grass', 'Fairy', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Ground', 'Poison', 'Rock', 'Electric', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Rock', 'Normal', 'Flying', 'Poison', 'Fire', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Bug', 'Fighting', 'Ground', 'Grass', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Ghost', 'Normal', 'Fighting', 'Poison', 'Bug', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Steel', 'Normal', 'Flying', 'Poison', 'Rock', 'Bug', 'Steel', 'Grass', 'Psychic', 'Ice', 'Dragon', 'Fairy'),
('Fire', 'Bug', 'Steel', 'Fire', 'Grass', 'Ice', NULL, NULL, NULL, NULL, NULL, NULL),
('Water', 'Steel', 'Fire', 'Water', 'Ice', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Grass', 'Ground', 'Water', 'Grass', 'Electric', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Electric', 'Flying', 'Steel', 'Electric', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Psychic', 'Fighting', 'Psychic', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Ice', 'Ice', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Dragon', 'Fire', 'Water', 'Grass', 'Electric', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Fairy', 'Fighting', 'Bug', 'Dragon', 'Dark', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
('Dark', 'Ghost', 'Psychic', 'Dark', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `strongagainst`
--

CREATE TABLE `strongagainst` (
  `Type` text NOT NULL,
  `StrongAgainst1` text,
  `StrongAgainst2` text,
  `StrongAgainst3` text,
  `StrongAgainst4` text,
  `StrongAgainst5` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `strongagainst`
--

INSERT INTO `strongagainst` (`Type`, `StrongAgainst1`, `StrongAgainst2`, `StrongAgainst3`, `StrongAgainst4`, `StrongAgainst5`) VALUES
('Normal', NULL, NULL, NULL, NULL, NULL),
('Fighting', 'Normal', 'Rock', 'Steel', 'Ice', 'Dark'),
('Flying', 'Fighting', 'Bug', 'Grass', NULL, NULL),
('Poison', 'Grass', 'Fairy', NULL, NULL, NULL),
('Ground', 'Poison', 'Rock', 'Steel', 'Fire', 'Electric'),
('Rock', 'Flying', 'Bug', 'Fire', 'Ice', NULL),
('Bug', 'Grass', 'Psychic', 'Dark', NULL, NULL),
('Ghost', 'Ghost', 'Phychic', NULL, NULL, NULL),
('Steel', 'Rock', 'Ice', 'Fairy', NULL, NULL),
('Fire', 'Bug', 'Steel', 'Grass', 'Ice', NULL),
('Water', 'Ground', 'Rock', 'Fire', NULL, NULL),
('Grass', 'Ground', 'Rock', 'Water', NULL, NULL),
('Electric', 'Flying', 'Water', NULL, NULL, NULL),
('Psychic', 'Fighting', 'Poison', NULL, NULL, NULL),
('Ice', 'Flying', 'Ground', 'Grass', 'Dragon', NULL),
('Dragon', 'Dragon', NULL, NULL, NULL, NULL),
('Fairy', 'Fighting', 'Dragon', 'Dark', NULL, NULL),
('Dark', 'Ghost', 'Psychic', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vulnerableto`
--

CREATE TABLE `vulnerableto` (
  `Type` text NOT NULL,
  `vulnerableTo1` text,
  `vulnerableTo2` text,
  `vulnerableTo3` text,
  `vulnerableTo4` text,
  `vulnerableTo5` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vulnerableto`
--

INSERT INTO `vulnerableto` (`Type`, `vulnerableTo1`, `vulnerableTo2`, `vulnerableTo3`, `vulnerableTo4`, `vulnerableTo5`) VALUES
('Normal', 'Fighting', NULL, NULL, NULL, NULL),
('Fighting', 'Flying', 'Psychic', 'Fairy', NULL, NULL),
('Flying', 'Rock', 'Electric', 'Ice', NULL, NULL),
('Poison', 'Ground', 'Psychic', NULL, NULL, NULL),
('Ground', 'Water', 'Grass', 'Ice', NULL, NULL),
('Rock', 'Fighting', 'Ground', 'Steel', 'Water', 'Grass'),
('Bug', 'Flying', 'Rock', 'Fire', NULL, NULL),
('Ghost', 'Ghost', 'Dark', NULL, NULL, NULL),
('Steel', 'Fighting', 'Ground', 'Fire', NULL, NULL),
('Fire', 'Ground', 'Rock', 'Water', NULL, NULL),
('Water', 'Grass', 'Electric', NULL, NULL, NULL),
('Grass', 'Flying', 'Poison', 'Bug', 'Fire', 'Ice'),
('Electric', 'Ground', NULL, NULL, NULL, NULL),
('Psychic', 'Bug', 'Ghost', 'Dark', NULL, NULL),
('Ice', 'Fighting', 'Rock', 'Steel', 'Fire', NULL),
('Dragon', 'Ice', 'Dragon', 'Fairy', NULL, NULL),
('Fairy', 'Poison', 'Steel', NULL, NULL, NULL),
('Dark', 'Fighting', 'Bug', 'Fairy', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `weakagainst`
--

CREATE TABLE `weakagainst` (
  `Type` text NOT NULL,
  `weakAgainst1` text,
  `weakAgainst2` text,
  `weakAgainst3` text,
  `weakAgainst4` text,
  `weakAgainst5` text,
  `weakAgainst6` text,
  `weakAgainst7` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weakagainst`
--

INSERT INTO `weakagainst` (`Type`, `weakAgainst1`, `weakAgainst2`, `weakAgainst3`, `weakAgainst4`, `weakAgainst5`, `weakAgainst6`, `weakAgainst7`) VALUES
('Normal', 'Rock', 'Ghost', 'Steel', NULL, NULL, NULL, NULL),
('Fighting', 'Flying', 'Poison', 'Psychic', 'Bug', 'Ghost', 'Fairy', NULL),
('Flying', 'Rock', 'Steel', 'Electric', NULL, NULL, NULL, NULL),
('Poison', 'Poison', 'Ground', 'Rock', 'Ghost', 'Steel', NULL, NULL),
('Ground', 'Flying', 'Bug', 'Grass', NULL, NULL, NULL, NULL),
('Rock', 'Fighting', 'Ground', 'Steel', NULL, NULL, NULL, NULL),
('Bug', 'Fighting', 'Flying', 'Poison', 'Ghost', 'Steel', 'Fire', 'Fairy'),
('Ghost', 'Normal', 'Dark', NULL, NULL, NULL, NULL, NULL),
('Steel', 'Steel', 'Fire', 'Water', 'Electric', NULL, NULL, NULL),
('Fire', 'Rock', 'Fire ', 'Water', 'Dragon', NULL, NULL, NULL),
('Water', 'Water', 'Grass', 'Dragon', NULL, NULL, NULL, NULL),
('Grass', 'Flying', 'Poison', 'Bug', 'Steel', 'Fire', 'Grass', 'Dragon'),
('Electric', 'Ground', 'Grass', 'Electric', 'Dragon', NULL, NULL, NULL),
('Phychic', 'Steel', 'Phychic', 'Dark', NULL, NULL, NULL, NULL),
('Ice', 'Steel', 'Fire', 'Water', 'Ice', NULL, NULL, NULL),
('Dragon', 'Steel', 'Fairy', NULL, NULL, NULL, NULL, NULL),
('Fairy', 'Poison', 'Steel ', 'Fire', NULL, NULL, NULL, NULL),
('Dark', 'Fighting', 'Dark', 'Fairy', NULL, NULL, NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

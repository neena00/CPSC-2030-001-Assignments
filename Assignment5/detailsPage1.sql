DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Pk`()
    NO SQL
SELECT p.Name AS 'name', pt.type1 AS `t1`, pt.type2  As `t2`, p.`Nat.#` as `nat`
	FROM Pokemon p
	INNER JOIN pokemontype pt ON pt.Name = p.Name$$
DELIMITER ;
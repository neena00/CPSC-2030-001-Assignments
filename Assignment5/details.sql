DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Specifications`(IN `name` VARCHAR(45))
    NO SQL
SELECT distinct p.Name AS name, pt.type1 AS t1, pt.type2  As t2, p.`Nat.#` as nat, p.`Hoenn#` as hoeNum, p.HP as hp , p.Atk as atk, p.Def As def , p.SAt As sat , p.SDf as sdf , p.Spd As spd , p.BST as bst, resistantto1 as r1 , resistantto2 as r2, resistantto3 as r3 , resistantto4 as r4, resistantto5 as r5 , resistantto6 as r6, resistantto7 as r7 , resistantto8 as r8, resistantto9 as r9, resistantto10 as r10, resistantto11 as r11,strongagainst1 as s1,strongagainst2 as s2,strongagainst3 as s3,strongagainst4 as s4
				,strongagainst5 as s5, vulnerableto1 as v1 , vulnerableto2 as v2 , vulnerableto3 as v3 , vulnerableto4 as v4 , vulnerableto5 as v5, weakagainst1 as w1 , weakagainst2 as w2 , weakagainst3 as w3 , weakagainst4 as w4 , weakagainst5 as w5 , weakagainst6 as w6 , weakagainst7 as w7
				FROM pokemon p
				INNER JOIN pokemontype pt ON p.name = pt.name
				INNER JOIN weakagainst w ON pt.type1 = w.type or pt.type2 = w.type
				INNER JOIN resistantto r ON pt.type1 = r.type or pt.type2 = r.type
				INNER JOIN vulnerableto v ON pt.type1 = v.type or pt.type2 = v.type
				INNER JOIN strongagainst s ON pt.type1 = s.type or pt.type2 = s.type
				WHERE p.name = '$name'$$
DELIMITER ;